@extends('home')

@section('css')

@endsection

@section('page-toolbar')
	<div class="page-toolbar">
		<div class="page-toolbar-block">
			<div class="page-toolbar-title">Role</div>
			<div class="page-toolbar-subtitle">Manage all role here</div>
		</div>
		<div class="page-toolbar-block pull-right">
			<div class="widget-info widget-from">
				<button form="form1" type="submit" class="btn btn-sm btn-success">
					<i class="fa fa-save"></i> Create
				</button>
				<a href="{{URL::previous()}}" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</a>
			</div>
		</div>
	</div>
@endsection

@section('page-content')
		
    <div class="col-md-6">
		<div class="block">
			@if ($errors->has())
			<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					{{ $error }}<br>        
				@endforeach
			</div>
			@endif
			<div class="block-content">
			<form id="form1" class="form-vertival" method="post" action=""
			autocomplete="off">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
				<h2><strong>Create</strong> Role</h2>
				<div class="form-group">
					<label>Name:</label> <span class="help-block"></span>
					<input name="name" type="text" class="form-control"/>                                        
				</div>
				<div class="form-group">
					<label>Display Name:</label>
					<input name="display_name" type="text" class="form-control"/>
				</div>  
				<div class="form-group">
					<label>Description:</label>
					<input name="description" type="text" class="form-control"/>
				</div>
				
				<div class="form-group ">
				<div><label>Permission:</label></div>
                    @foreach ($permissions as $permission)
                    <label class="col-xs-2" style="text-align: center;">
                        <input class="control-label" type="hidden" id="permissions[{{{ $permission['id'] }}}]" name="permissions[{{{ $permission['id'] }}}]" value="0" />
                        <input class="form-control" type="checkbox" id="permissions[{{{ $permission['id'] }}}]" name="permissions[{{{ $permission['id'] }}}]" value="1"{{{ (isset($permission['checked']) && $permission['checked'] == true ? ' checked="checked"' : '')}}} />
                        {{{ $permission['display_name'] }}}
                    </label>
                    @endforeach
                </div>
				
			</form>
			</div>
		</div>

@endsection

@section('js')


@endsection

@section('scripts')



@endsection

@extends('app')

@section('content')
<div class="page-container">
            
            <div class="page-content page-content-default">

                <div class="block-login">
                    <div class="block-login-logo">
                        
                    </div>                    
                    <div class="block-login-content">
                        <h1>Sign in</h1>
							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
                        <form method="POST" role="form" action="{{ url('/auth/login') }}">
                        
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
                        <div class="form-group">                        
                            <input type="email" name="email" class="form-control" placeholder="Your login/e-mail" value="{{ old('email') }}"/>
                        </div>
                        <div class="form-group">                        
                            <input type="password" name="password" class="form-control" placeholder="Your password" value=""/>
                        </div>
                        <div class="pull-left">
                            <div class="checkbox">
                                <label><input type="checkbox" name="remember"/> Keep me signed in</label>
                            </div>
                        </div>                                        
                        <div class="pull-right">
                            <a href="pages-signin.html#">Forgot your password?</a>
                        </div>

                        <button class="btn btn-primary btn-block" type="submit">Sign in</button>                                        
                        
                        </form>
						
                        <div class="sp"></div>
                        <div class="pull-left">
                            © Nusantara Beta Studio
                        </div>
                    </div>

                </div>
                
            </div>
        </div>
        
        <script type="text/javascript">
        $("#signinForm").validate({
		rules: {
			login: "required",
			password: "required"
		},
		messages: {
			firstname: "Please enter your login",
			lastname: "Please enter your password"			
		}
	});            
        </script>
@endsection

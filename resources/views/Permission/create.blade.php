@extends('home')

@section('css')

@endsection

@section('page-toolbar')
	<div class="page-toolbar">
		<div class="page-toolbar-block">
			<div class="page-toolbar-title">Permission</div>
			<div class="page-toolbar-subtitle">Manage all permission here</div>
		</div>
		<div class="page-toolbar-block pull-right">
			<div class="widget-info widget-from">
				<button form="form1" type="submit" class="btn btn-sm btn-success">
					<i class="glyphicon glyphicon-ok-circle"></i> Create
				</button>
				<a href="{{URL::previous()}}" class="btn btn-primary"><i class="fa fa-pencil"></i> Cancel</a>
			</div>
		</div>
	</div>
@endsection

@section('page-content')
		
    <div class="col-md-6">
		<div class="block">
			@if ($errors->has())
			<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					{{ $error }}<br>        
				@endforeach
			</div>
			@endif
			<div class="block-content">
			<form id="form1" class="form-vertival" method="post" action=""
			autocomplete="off">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
				<h2><strong>Create</strong> Permission</h2>
				<div class="form-group">
					<label>Name:</label> <span class="help-block"></span>
					<input name="name" type="text" class="form-control"/>                                        
				</div>
				<div class="form-group">
					<label>Display Name</label>
					<input name="display_name" type="text" class="form-control"/>
				</div>  
				<div class="form-group">
					<label>Description</label>
					<input name="description" type="text" class="form-control"/>
				</div>
				
			</form>
			</div>
		</div>

@endsection

@section('js')


@endsection

@section('scripts')



@endsection

@extends('home')

@section('css')


@endsection

@section('page-toolbar')
	<div class="page-toolbar">
		<div class="page-toolbar-block">
            <div class="page-toolbar-title">Employee Performance</div>
            <div class="page-toolbar-subtitle">Employee performance dashboard</div>
        </div>
                        
        <div class="page-toolbar-block pull-right">
            <div class="widget-info widget-from">
            <div id="reportrange" class="dtrange">                                            
            <span></span><b class="caret"></b>
            </div>                            
        </div>
        </div>
		<ul class="page-toolbar-tabs">
			<li class="active"><a href="#page-tab-1">MAIN</a></li>
			<li><a href="#page-tab-2">Region 1</a></li>
			<li><a href="#page-tab-3">Region 2</a></li>
			<li><a href="#page-tab-4">Region 3</a></li>
		</ul>
	</div>
@endsection

@section('page-content')
<div class="row page-toolbar-tab active" id="page-tab-1">
	<div class="row">

	<div class="col-md-6">                    
		<div class="block">
			<div class="block-content">
				<h2><strong>Top</strong> VM <span class="fa fa-level-up"></span></h2>
			</div>
			<div class="block-content np">
				<table class="table table-striped">
					<tbody><tr>
						<th>No</th><th>Name</th><th>Visibility</th><th>Points</th>
					</tr>
					<tr class='clickable-row' data-href="{{ URL::to('performance/1') }}">
						<td>1st</td><td>Supriyadi</td><td>98%</td><td>3115</td>
					</tr>
					<tr>
						<td>2nd</td><td>Yanggi Agustiana</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>3rd</td><td>Asep Maulana</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>4th</td><td>Muhammad Arsyad Al Banjari</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>5th</td><td>Suprayitno</td><td>Null</td><td>Null</td>
					</tr>                                        
				</tbody></table>
			</div>
		</div>
	</div>
	<div class="col-md-6">                    
		<div class="block">
			<div class="block-content">
				<h2><strong>Top</strong> SI <span class="fa fa-level-up"></span></h2>
			</div>                                
			<div class="block-content np">
				<table class="table table-striped">
					<tbody><tr>
						<th>No</th><th>Name</th><th>Training</th><th>Points</th>
					</tr>
					<tr>
						<td>1st</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>2nd</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>3rd</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>4th</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>5th</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>                                        
				</tbody></table>
			</div>
		</div>
	</div>
	</div>
	
	<div class="row">
	
	<div class="col-md-6">                    
		<div class="block">
			<div class="block-content">
				<h2><strong>Bottom</strong> VM <span class="fa fa-level-down"></span></h2>
			</div>                                
			<div class="block-content np">
				<table class="table table-striped">
					<tbody><tr>
						<th>No</th><th>Name</th><th>Visibility</th><th>Points</th>
					</tr>
					<tr>
						<td>23rd</td><td>Fikri Pratomo</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>24th</td><td>Bagus</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>25th</td><td>Endi Kurnia </td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>27th</td><td>Susanta Krisna Pamungkas</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>28th</td><td>Donny Dharyanto</td><td>Null</td><td>Null</td>
					</tr>                                        
				</tbody></table>
			</div>
		</div>
	</div>	
	<div class="col-md-6">                    
		<div class="block">
			<div class="block-content">
				<h2><strong>Bottom</strong> SI <span class="fa fa-level-down"></span></h2>
			</div>                                
			<div class="block-content np">
				<table class="table table-striped">
					<tbody><tr>
						<th>No</th><th>Name</th><th>Sales</th><th>Points</th>
					</tr>
					<tr>
						<td>Null</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>Null</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>null</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>null</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>
					<tr>
						<td>null</td><td>Null</td><td>Null</td><td>Null</td>
					</tr>                                        
				</tbody></table>
			</div>
		</div>
	</div>		
	</div>
</div>


<div class="row page-toolbar-tab" id="page-tab-2">
	
</div>


@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/knob/jquery.knob.js') }}"></script>
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/demo.js') }}"></script>	
@endsection

@section('scripts')

<script type="text/javascript">
    $(function(){
      $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
		});
    });
</script>

@endsection

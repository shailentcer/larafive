@extends('home')

@section('css')

@endsection

@section('page-toolbar')
	<div class="page-toolbar">
		<div class="page-toolbar-block">
			<div class="page-toolbar-title">Users</div>
			<div class="page-toolbar-subtitle">Manage all users here</div>
		</div>
		<div class="page-toolbar-block pull-right">
			<div class="widget-info widget-from">
				<button form="form1" type="submit" class="btn btn-sm btn-success">
					<i class="fa fa-save"></i> Create
				</button>
				<a href="{{URL::to('/user')}}" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</a>
			</div>
		</div>
	</div>
@endsection

@section('page-content')
		
    <div class="col-md-6">
		<div class="block">
			<div class="block-content">
			<h2><strong>Create</strong> User</h2>
			@if ($errors->has())
			<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					{{ $error }}<br>        
				@endforeach
			</div>
			@endif
			<form id="form1" class="form-vertival" method="post" action=""
			autocomplete="off">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
				
				<div class="form-group">
					<label>Name:</label> <span class="help-block"></span>
					<input required="required" name="name" type="text" class="form-control"/>                                        
				</div>
				<div class="form-group">
					<label>Email:</label>
					<input required="required"  name="email" type="text" class="form-control"/>
				</div>  
				<div class="form-group">
					<label>Password:</label>
					<input required="required"  name="password" type="password" class="form-control"/>
				</div>
				<div class="form-group">
					<label>Verify Password:</label>
					<input required="required" name="password2" type="password" class="form-control"/>
				</div>
				<div class="form-group">
					<div><label>Role:</label></div>
					<select name="role_id" required="required">
					<option selected="selected" value="" disabled="disabled">Choose role</option>
					@foreach($roles as $role)
					 <option value="{{$role['id']}}">{{$role['name']}}</option>
					@endforeach
					</select>
				</div>
				<div class="form-group">
					<div><label>Accessibility:</label></div>
					<label class="col-xs-4" style="text-align: center;">Web Access
					<input class="control-label" type="hidden"  name="web_access" value="0" />
					<input class="form-control" type="checkbox"  name="web_access" value="1" />
					</label>
					
					<label class="col-xs-4" style="text-align: center;">Mobile Access
					<input class="control-label" type="hidden"  name="mobile_access" value="0" />
					<input class="form-control" type="checkbox"  name="mobile_access" value="1" />
					</label>
					
					<label class="col-xs-4" style="text-align: center;">User Activated
					<input class="control-label" type="hidden"  name="activated" value="0" />
					<input class="form-control" type="checkbox"  name="activated" value="1" />
					</label>
				</div>
			</form>
			</div>
		</div>

@endsection

@section('js')


@endsection

@section('scripts')



@endsection

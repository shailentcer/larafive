@extends('home')

@section('css')


@endsection

@section('page-toolbar')
	<div class="page-toolbar">
		<div class="page-toolbar-block">
            <div class="page-toolbar-title">Dashboard</div>
            <div class="page-toolbar-subtitle">Exclusive responsive dashboard</div>
        </div>
                        
        <div class="page-toolbar-block pull-right">
            <div class="widget-info widget-from">
            <div id="reportrange" class="dtrange">                                            
            <span></span><b class="caret"></b>
            </div>                            
        </div>
        </div>
	</div>
@endsection

@section('page-content')
	
	<!-- div class="block-wide">                       
                <div class="block-wide-title">Latest Visits</div>                        
                <div id="dashboard-chart" style="height: 250px; width: 100%; float: left;"></div>
                <div class="chart-legend">
            <div id="dashboard-legend"></div>
        </div>
    </div -->
	
	<div class="row">
                        
	<div class="col-md-4">
		<div class="widget-pie">
			<div class="knob">
				<input type="text" data-fgColor="#334454" data-bgColor="#FFFFFF" data-min="0" data-max="100" data-width="90" data-height="90" value="95"/>
			</div>
			<div class="widget-pie-info">
				<div class="widget-pie-info-num">5,352</div>
				<div class="widget-pie-info-text">Total Visits</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="widget-pie">
			<div class="knob">
				<input type="text" data-fgColor="#8CC152" data-bgColor="#FFFFFF" data-min="0" data-max="100" data-width="90" data-height="90" value="75"/>
			</div>
			<div class="widget-pie-info">
				<div class="widget-pie-info-num">1,582</div>
				<div class="widget-pie-info-text">New Visitors</div>
			</div>
		</div>                            
	</div>
	<div class="col-md-4">
		<div class="widget-pie">
			<div class="knob">
				<input type="text" data-fgColor="#F6BB42" data-bgColor="#FFFFFF" data-min="0" data-max="100" data-width="90" data-height="90" value="25"/>
			</div>
			<div class="widget-pie-info">
				<div class="widget-pie-info-num">3,770</div>
				<div class="widget-pie-info-text">Returned Visitors</div>
			</div>
		</div>                            
	</div>                        
</div>
	
	
	
	<div class="row">
                        <div class="col-md-12">
                            <div class="q-buttons">
                                <a href="pages-mailbox-inbox.html" class="q-button">                                    
                                    <div class="q-button-icon">
                                        <i class="fa fa-envelope-o"></i>
                                    </div>
                                    <div class="q-button-text">E-mail</div>                                    
                                    <div class="informer informer-danger">8</div>
                                </a>
                                <a href="charts.html" class="q-button">
                                    <div class="q-button-icon">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="q-button-text">Charts</div>
                                    <div class="informer informer-info">2</div>
                                </a>
                                <a href="maps-google.html" class="q-button">
                                    <div class="q-button-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="q-button-text">Maps</div>
                                    <div class="informer informer-success">!</div>
                                </a>
                                <a href="form-elements.html" class="q-button">
                                    <div class="q-button-icon">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    <div class="q-button-text">Forms</div>
                                    <div class="informer informer-warning">1</div>
                                </a>
                                <a href="ui-buttons.html" class="q-button">
                                    <div class="q-button-icon">
                                        <i class="fa fa-crosshairs"></i>
                                    </div>
                                    <div class="q-button-text">Buttons</div>                                    
                                    <div class="informer informer-pulsate informer-info">5</div>
                                </a>
                                <a href="pages-timeline.html" class="q-button">
                                    <div class="q-button-icon">
                                        <i class="fa fa-location-arrow"></i>
                                    </div>
                                    <div class="q-button-text">Timeline</div>
                                    <div class="informer informer-pulsate informer-success">3</div>
                                </a>
                                <a href="pages-invoice.html" class="q-button">
                                    <div class="q-button-icon">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                    <div class="q-button-text">Invoice</div>
                                    <div class="informer informer-pulsate informer-warning">7</div>
                                </a>
                                <a href="pages-signin.html" class="q-button">
                                    <div class="q-button-icon">
                                        <i class="fa fa-sign-in"></i>
                                    </div>
                                    <div class="q-button-text">Sign In</div>
                                    <div class="informer informer-pulsate informer-danger">6</div>
                                </a>
                            </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-container">
                                    <div class="widget-text">Invoices statistic</div>
                                    <div class="widget-chart">
                                        <span class="sparkline" sparkType="bar" sparkBarColor="white" sparkHeight="30" sparkBarWidth="5">130,190,260,230,290,400,340,360,390</span>
                                    </div>
                                </div>
                                <div class="widget-content">
                                    <div class="widget-text"><strong>Amount paid</strong> $1,530</div>
                                    <div class="widget-text"><strong>In queue</strong> $2,102</div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget widget-success">
                                <div class="widget-container">
                                    <div class="widget-text">Last visits</div>
                                    <div class="widget-chart">
                                        <span class="sparkline" sparkType="line" sparkLineColor="white" sparkHeight="30" sparkWidth="70" sparkLineWidth="2" sparkFillColor="false">130,190,260,230,290,400,340,360,390</span>
                                    </div>
                                </div>
                                <div class="widget-content">
                                    <div class="widget-text"><strong>New</strong> 65%</div>
                                    <div class="widget-text"><strong>Returning</strong> 35%</div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget widget-warning">
                                <div class="widget-container">
                                    <div class="widget-text">Visitors</div>
                                    <div class="widget-chart">
                                        <span class="sparkline" sparkType="pie"sparkHeight="30">130,190</span>
                                    </div>
                                </div>
                                <div class="widget-content">
                                    <div class="widget-text"><strong>Returned</strong> 190</div>
                                    <div class="widget-text"><strong>New</strong> 130</div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget widget-info">
                                <div class="widget-container">
                                    <div class="widget-text">Invoices statistic</div>
                                    <div class="widget-chart">
                                        <span class="sparkline" sparkType="discrete" sparkLineColor="#FFFFFF" sparkWidth="70" sparkHeight="30">130,190,260,230,290,400,340,360,390</span>
                                    </div>
                                </div>
                                <div class="widget-content">
                                    <div class="widget-text"><strong>Total</strong> $2,530</div>
                                    <div class="widget-text"><strong>Pre week</strong> $1,102</div>                                    
                                </div>
                            </div>
                        </div>
                    </div>                        
                    
                    <div class="row">
                        <div class="col-md-4">
                            
                            <div class="widget-window">
                                <div class="window window-success window-npb">
                                    <div class="window-title">Latest Visits</div> 
                                </div>
                                <div class="window window-success tac">
                                    <span class="sparkline" sparkType="line" sparkHighlightSpotColor="#FFF" sparkSpotRadius="5" sparkMaxSpotColor="#FFFFFF" sparkMinSpotColor="#FFFFFF" sparkSpotColor="#FFFFFF" sparkLineColor="#FFFFFF" sparkHeight="100" sparkWidth="100%" sparkLineWidth="3" sparkFillColor="false">130,190,260,230,290,400,340,360,390</span>
                                </div>
                                <div class="window window-dark">
                                    <div class="window-block">
                                        <h4>Total Visits</h4>
                                        <p>1,954</p>                                    
                                        <h4>Returned Visitors</h4>
                                        <p>1,250</p>
                                        <h4>New Visitors</h4>
                                        <p>704</p>
                                    </div>
                                    <div class="window-block pull-right">
                                        <div class="knob">
                                            <input type="text" data-fgColor="#8CC152" data-min="0" data-max="100" data-width="100" data-height="100" value="15"/>
                                        </div>                                                  
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            
                            <div class="widget-window">
                                <div class="window window-danger window-npb">
                                    <div class="window-title">Latest Visits</div> 
                                </div>
                                <div class="window window-danger tac">
                                    <span class="sparkline" sparkType="line" sparkHighlightSpotColor="#FFF" sparkSpotRadius="5" sparkMaxSpotColor="#FFFFFF" sparkMinSpotColor="#FFFFFF" sparkSpotColor="#FFFFFF" sparkLineColor="#FFFFFF" sparkHeight="100" sparkWidth="100%" sparkLineWidth="3" sparkFillColor="false">130,190,260,230,290,400,340,360,390</span>
                                </div>
                                <div class="window">
                                    <div class="window-block">
                                        <h4>Total Visits</h4>
                                        <p>1,954</p>                                    
                                        <h4>Returned Visitors</h4>
                                        <p>1,250</p>
                                        <h4>New Visitors</h4>
                                        <p>704</p>
                                    </div>
                                    <div class="window-block pull-right">
                                        <div class="knob">
                                            <input type="text" data-fgColor="#DA4453" data-min="0" data-max="100" data-width="100" data-height="100" value="45"/>
                                        </div>                                                  
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            
                            <div class="widget-window">
                                <div class="window window-info window-npb">
                                    <div class="window-title">Latest Visits</div> 
                                </div>
                                <div class="window window-info tac">
                                    <span class="sparkline" sparkType="line" sparkHighlightSpotColor="#FFF" sparkSpotRadius="5" sparkMaxSpotColor="#FFFFFF" sparkMinSpotColor="#FFFFFF" sparkSpotColor="#FFFFFF" sparkLineColor="#FFFFFF" sparkHeight="100" sparkWidth="100%" sparkLineWidth="3" sparkFillColor="false">130,190,260,230,290,400,340,360,390</span>
                                </div>
                                <div class="window window-primary">
                                    <div class="window-block">
                                        <h4>Total Visits</h4>
                                        <p>1,954</p>                                    
                                        <h4>Returned Visitors</h4>
                                        <p>1,250</p>
                                        <h4>New Visitors</h4>
                                        <p>704</p>
                                    </div>
                                    <div class="window-block pull-right">
                                        <div class="knob">
                                            <input type="text" data-fgColor="#FFF" data-min="0" data-max="100" data-width="100" data-height="100" value="75"/>
                                        </div>                                                  
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4">
                            
                            <div class="widget-window">
                                <div class="window window-primary window-npb">
                                    <div class="window-title">John Doe</div> 
                                </div>
                                <div class="window window-primary">
                                    <div class="window-block">
                                        <img src="img/samples/users/user-60.jpg" class="img-circle img-thumbnail"/>
                                    </div>
                                    <div class="window-block">
                                        <h4>Web Developer</h4>
                                        <p>With expirience 7 years</p>
                                        <h4>Knowlage</h4>
                                        <p>HTML,CSS,JS,PHP,MySQL</p>
                                    </div>
                                </div>
                                <div class="window">
                                    <div class="window-block window-wide">
                                        <h4>HTML</h4>                                        
                                        <div class="progress progress-small">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 90%"></div>
                                        </div>
                                        <h4>CSS</h4>                                        
                                        <div class="progress progress-small">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 85%"></div>
                                        </div>
                                        <h4>JS</h4>                                        
                                        <div class="progress progress-small">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 65%"></div>
                                        </div>
                                        <h4>PHP</h4>                                        
                                        <div class="progress progress-small">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%"></div>
                                        </div>
                                        <h4>MySQL</h4>                                        
                                        <div class="progress progress-small">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 80%"></div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            
                            <div class="widget-window">
                                <div class="window window-info window-npb">
                                    <div class="window-title">John Doe</div> 
                                </div>
                                <div class="window window-info">
                                    <div class="window-block">
                                        <img src="img/samples/users/user-60.jpg" class="img-circle img-thumbnail"/>
                                    </div>
                                    <div class="window-block">
                                        <h4>Actor</h4>
                                        <p>With expirience 20 years</p>
                                        <h4>Followers</h4>
                                        <p>1,544,232</p>
                                    </div>
                                </div>
                                <div class="window">
                                    <div class="window-block window-wide">
                                        <div class="form-group">
                                            <textarea class="form-control" style="height: 90px;"></textarea>
                                        </div>
                                        <div class="form-group pull-left nmb">
                                            <button class="btn btn-info"><i class="fa fa-camera"></i></button> 
                                            <button class="btn btn-info"><i class="fa fa-map-marker"></i></button>    
                                        </div>
                                        <div class="form-group pull-right">
                                            <button class="btn btn-info">Send</button>    
                                        </div>
                                    </div>                                    
                                </div>                                
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            
                            <div class="widget-window">
                                <div class="window window-dark window-npb">
                                    <div class="window-title">John Doe</div> 
                                </div>
                                <div class="window window-dark">
                                    <div class="window-block">
                                        <img src="img/samples/users/user-60.jpg" class="img-circle img-thumbnail"/>
                                    </div>
                                    <div class="window-block">
                                        <h4>Producer</h4>
                                        <p>With expirience 30 years</p>
                                        <h4>Followers</h4>
                                        <p>644,232</p>
                                    </div>
                                </div>
                                <div class="window">
                                    <div class="window-block window-wide scroll" style="height: 135px;">
                                        <h4>About</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis tristique tortor. Duis non ligula placerat, dapibus arcu ac, blandit purus. Suspendisse eu aliquam odio. Suspendisse fringilla urna sed nisi pretium viverra vel id erat. Nam mollis augue quis ornare venenatis. Mauris a consectetur diam. Nulla luctus libero sed erat pulvinar, non cursus velit luctus. Duis eget enim tellus. Integer id velit et nisi rhoncus scelerisque. Phasellus condimentum porttitor neque quis adipiscing.</p>
                                        <p>Vestibulum volutpat neque faucibus mauris bibendum vulputate. Cras enim elit, molestie non urna vel, facilisis dapibus diam. Vestibulum sit amet leo adipiscing, imperdiet metus eu, commodo dolor. Cras dapibus risus ipsum, ac convallis magna consequat vitae. In bibendum ultricies tempus. Aenean at malesuada justo, vel posuere augue. Nullam ipsum magna, volutpat sit amet posuere non, feugiat interdum felis. Suspendisse ultrices at quam eu sagittis. Fusce rutrum mi lectus, et dignissim tellus vehicula eget. Praesent pulvinar suscipit elit id tristique. Donec ultrices lorem eu dui dictum, nec gravida ipsum fermentum. In quis bibendum sem.</p>
                                    </div>                                    
                                </div>                                
                            </div>
                            
                        </div>
                    </div>
        <div class="col-md-12">
		 <div class="block">
			<div class="block-head">
				<h2>Datatable All Features</h2>
			</div>
			<div class="block-content np">
				<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
					<thead>
						<tr>
							<th><input type="checkbox" class="checkall"/></th>
							<th width="25%">ID</th>
							<th width="25%">Name</th>
							<th width="25%">E-mail</th>
							<th width="25%">Phone</th>                                    
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>101</td>
							<td>Dmitry</td>
							<td>dmitry@domain.com</td>
							<td>+98(765)432-10-98</td>                                    
						</tr>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>102</td>
							<td>Alex</td>
							<td>alex@domain.com</td>
							<td>+98(765)432-10-99</td>                                    
						</tr>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>103</td>
							<td>John</td>
							<td>john@domain.com</td>
							<td>+98(765)432-10-97</td>                                    
						</tr>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>104</td>
							<td>Angelina</td>
							<td>angelina@domain.com</td>
							<td>+98(765)432-10-90</td>                                    
						</tr>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>105</td>
							<td>Tom</td>
							<td>tom@domain.com</td>
							<td>+98(765)432-10-92</td>                                    
						</tr>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>106</td>
							<td>Helen</td>
							<td>helen@domain.com</td>
							<td>+98(765)432-11-33</td>                                    
						</tr>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>107</td>
							<td>Aqvatarius</td>
							<td>aqvatarius@domain.com</td>
							<td>+98(765)432-15-66</td>                                    
						</tr>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>108</td>
							<td>Olga</td>
							<td>olga@domain.com</td>
							<td>+98(765)432-11-97</td>                                    
						</tr>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>109</td>
							<td>Homer</td>
							<td>homer@domain.com</td>
							<td>+98(765)432-11-90</td>                                    
						</tr>
						<tr>
							<td><input type="checkbox" name="checkbox"/></td>
							<td>110</td>
							<td>Tifany</td>
							<td>tifany@domain.com</td>
							<td>+98(765)432-11-92</td>                                    
						</tr>                                
					</tbody>
				</table>                                        

			</div>
		</div>
		</div>
		
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
	<script type="text/javascript" src="{{asset('themes/gemini/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
	 <script type="text/javascript" src="{{asset('themes/gemini/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
	<script type='text/javascript' src="{{ asset('themes/gemini/js/plugins/knob/jquery.knob.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/demo.js') }}"></script>	
@endsection

@section('scripts')



@endsection

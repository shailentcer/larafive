@extends('app')

@section('content')
	<div class="page-container">
            
            <div class="page-head">
                
                <ul class="page-head-elements">
                    <li><a href="#" class="page-navigation-toggle"><span class="fa fa-bars"></span></a></li>
                    <li class="search">
                        <input type="text" class="form-control" placeholder="Search..."/>
                    </li>
                </ul>

                
                
            </div>
            
            <div class="page-navigation">
                
                <div class="profile">                    
                    <img width="30" src="http://www.gravatar.com/avatar/?d=identicon"/>
                    <div class="profile-info">
                        <a href="" class="profile-title">Hello, {{Auth::user()->name}}</a>
                        <span class="profile-subtitle">Administrator</span>
						<div class="profile-buttons ">
                            <div class="btn-group pull-right">                                
                                <a class="but dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Profile</a></li>
									<li class="divider"></li>
									<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                                </ul>
                            </div>
                        </div>  
                    </div>
                </div>

                <ul class="navigation">
                    <li class="{{ Request::is('/') ? 'active' : '' }}">
                        <a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard </a>
                    </li>                    
                </ul>
				<ul class="navigation">
                    <li class="{{ Request::is('performance') ? 'active' : '' }}">
                        <a href="{{URL::to('/performance')}}"><i class="fa fa-bar-chart-o"></i> Performance </a>
                    </li>                    
                </ul>
				<ul class="navigation">
                    <li class="{{ Request::is('users') ? 'active' : '' }}">
                        <a href="#"><i class="fa fa-users"></i> User Management </a>
						<ul>
                            <li><a href="{{URL::to('/user')}}">Users</a></li>
                            <li><a href="{{URL::to('/role')}}">Roles</a></li>
                            <li><a href="{{URL::to('/permission')}}">Permissions</a></li>
                        </ul>
                    </li>                    
                </ul>
				<ul class="navigation">
                    <li class="{{ Request::is('master') ? 'active' : '' }}">
                        <a href="#"><i class="fa fa-briefcase"></i> Master Data </a>
						<ul>
							<li><a href="{{URL::to('/regional')}}">Regional</a></li>
                            <li><a href="{{URL::to('/kota')}}">Kota</a></li>
                            <li><a href="{{URL::to('/activityvisibility')}}">Activity Visibility</a></li>
							<li><a href="{{URL::to('/subactivityvisibility')}}">SubActivity Visibility</a></li>
							<li><a href="{{URL::to('/kategoritraining')}}">Kategori Training</a></li>
							<li><a href="{{URL::to('/materitraining')}}">Materi Training</a></li>
							<li><a href="{{URL::to('/kategorichannel')}}">Kategori Channel</a></li>
							<li><a href="{{URL::to('/channel')}}">Channel</a></li>
							<li><a href="{{URL::to('/store')}}">Store</a></li>
							<li><a href="{{URL::to('/rsp')}}">RSP</a></li>
							<li><a href="{{URL::to('/vm')}}">VM</a></li>
							<li><a href="{{URL::to('/brand')}}">Brand</a></li>
							<li><a href="">Zone</a></li>
                        </ul>
                    </li>                    
                </ul>
				<ul class="navigation">
                    <li class="{{ Request::is('monitoring') ? 'active' : '' }}">
                        <a href="#"><i class="fa fa-camera"></i> Monitoring </a>
						<ul>
                            <li><a href="#">Visibility</a></li>
                            <li><a href="#">Training</a></li>
							<li><a href="#">Daily Visibility</a></li>
							<li><a href="#">Daily Training</a></li>
                        </ul>
                    </li>                    
                </ul>
                <ul class="navigation">
                    <li class="{{ Request::is('report') ? 'active' : '' }}">
                        <a href="#"><i class="fa fa-print"></i> Report </a>
						<ul>
                            <li><a href="#">Data Master</a></li>
                            <li><a href="#">Visibility</a></li>
                            <li><a href="#">Akun</a></li>
							<li><a href="#">ARCS</a></li>
                        </ul>
                    </li>                    
                </ul>
            </div>
            
            <div class="page-content">
				
                <div class="container">
                    
					@yield('page-toolbar')                 
                    
					<!-- CONTENT HERE -->
					@yield('page-content')
				</div>
					
                    
                </div>
            </div>            
        </div>
@endsection
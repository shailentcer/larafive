<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Larafive</title>

	<link href="{{ asset('themes/gemini/css/styles.css') }}" rel="stylesheet">
	@yield('css')
	<!--[if lt IE 10]><link rel="stylesheet" type="text/css" href="themes/gemini/css/ie.css"/><![endif]-->
	
	<!-- Fonts -->
	<!-- link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css' -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	
    @yield('content')
	
	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/jquery/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/jquery/jquery-ui.min.js') }}"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
        
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins.js') }}"></script>
	<script type="text/javascript" src="{{ asset('themes/gemini/js/actions.js') }}"></script>   

	@yield('js')
	
	
	
	@yield('scripts')

	
</body>
</html>

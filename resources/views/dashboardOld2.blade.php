@extends('home')

@section('css')


@endsection

@section('page-toolbar')
	<div class="page-toolbar">
		<div class="page-toolbar-block">
            <div class="page-toolbar-title">Dashboard</div>
            <div class="page-toolbar-subtitle">Exclusive responsive dashboard</div>
        </div>
                        
        <div class="page-toolbar-block pull-right">
            <div class="widget-info widget-from">
            <div id="reportrange" class="dtrange">                                            
            <span></span><b class="caret"></b>
            </div>                            
        </div>
        </div>
		<ul class="page-toolbar-tabs">
			<li class="active"><a href="#page-tab-1">ALL</a></li>
			<li><a href="#page-tab-2">Region 1</a></li>
			<li><a href="#page-tab-3">Region 2</a></li>
			<li><a href="#page-tab-4">Region 3</a></li>
		</ul>
	</div>
@endsection

@section('page-content')
<div class="row page-toolbar-tab active" id="page-tab-1">
	<div class="row">
		<div class="col-md-6">
		<div class="widget-window">
		<div class="window" style="padding: 0px;">
			<div id="vector_map" style="height: 250px;"></div>
		</div>
			<div class="window window-dark">
				<div class="window-block">
					<h4>Orders</h4>
					<p>1,954</p>                                    
					<h4>Total Amount</h4>
					<p>1,543,987.65</p>
					<h4>In Queue</h4>
					<p>15</p>
				</div>
				<div class="window-block pull-right">
					<div class="knob">
						<input type="text" readOnly="true" data-fgColor="#8CC152" data-min="0" data-max="100" data-width="100" data-height="100" value="15"/>
					</div>                                                  
				</div>
			</div>
		</div>          
		</div>
		<div class="col-md-3">		
			<div class="widget-window">
				<div class="window window-success window-npb">
					<div class="window-title">Latest Visits</div> 
				</div>
				<div class="window window-success tac">
					<span class="sparkline" sparkType="line" sparkHighlightSpotColor="#FFF" sparkSpotRadius="5" sparkMaxSpotColor="#FFFFFF" sparkMinSpotColor="#FFFFFF" sparkSpotColor="#FFFFFF" sparkLineColor="#FFFFFF" sparkHeight="100" sparkWidth="100%" sparkLineWidth="3" sparkFillColor="false">130,190,260,230,290,400,340,360,390</span>
				</div>
				<div class="window window-dark">
					<div class="window-block">
						<h4>Total Visits</h4>
						<p>1,954</p>                                    
						<h4>Returned Visitors</h4>
						<p>1,250</p>
						<h4>New Visitors</h4>
						<p>704</p>
					</div>
					<div class="window-block pull-right">
						<div class="knob">
							<input type="text" data-fgColor="#8CC152" data-min="0" data-max="100" data-width="100" data-height="100" value="15"/>
						</div>                                                  
					</div>
				</div>
			</div>		
		</div>
		
		<div class="col-md-3">			
			<div class="widget-window">
				<div class="window window-info window-npb">
					<div class="window-title">Latest Visits</div> 
				</div>
				<div class="window window-info tac">
					<span class="sparkline" sparkType="line" sparkHighlightSpotColor="#FFF" sparkSpotRadius="5" sparkMaxSpotColor="#FFFFFF" sparkMinSpotColor="#FFFFFF" sparkSpotColor="#FFFFFF" sparkLineColor="#FFFFFF" sparkHeight="100" sparkWidth="100%" sparkLineWidth="3" sparkFillColor="false">130,190,260,230,290,400,340,360,390</span>
				</div>
				<div class="window window-primary">
					<div class="window-block">
						<h4>Total Visits</h4>
						<p>1,954</p>                                    
						<h4>Returned Visitors</h4>
						<p>1,250</p>
						<h4>New Visitors</h4>
						<p>704</p>
					</div>
					<div class="window-block pull-right">
						<div class="knob">
							<input type="text" data-fgColor="#FFF" data-min="0" data-max="100" data-width="100" data-height="100" value="75"/>
						</div>                                                  
					</div>
				</div>
			</div>		
		</div>
		
		<div class="col-md-3">
			<div class="widget widget-warning">
				<div class="widget-container">
					<div class="widget-text">Visitors</div>
					<div class="widget-chart">
						<span class="sparkline" sparkType="pie"sparkHeight="30">130,190</span>
					</div>
				</div>
				<div class="widget-content">
					<div class="widget-text"><strong>Returned</strong> 190</div>
					<div class="widget-text"><strong>New</strong> 130</div>                                    
				</div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="widget">
				<div class="widget-container">
					<div class="widget-text">Invoices statistic</div>
					<div class="widget-chart">
						<span class="sparkline" sparkType="bar" sparkBarColor="white" sparkHeight="30" sparkBarWidth="5">130,190,260,230,290,400,340,360,390</span>
					</div>
				</div>
				<div class="widget-content">
					<div class="widget-text"><strong>Amount paid</strong> $1,530</div>
					<div class="widget-text"><strong>In queue</strong> $2,102</div>                                    
				</div>
			</div>
		</div>
		
	</div><!--row closed-->	    	 
	
	<div class="row">
	<div class="col-md-8">                      
		<div class="block">
			<div class="block-content">
				<h2><strong>Sales </strong>Out</h2>
			</div>
			<div class="block-content np">
				<table class="table table-hover">
					<thead>				
					<tr>
						<th>City</th><th>#Acc</th><th>#Store</th><th>#Intel</th><th>%MSS</th><th>#QTD PQ</th><th>%QoQ</th><th>#QTD PY</th><th>%YoY</th>
					</tr>
					</thead>
					<tbody><tr><td class="has-link" data-link="1" data-column="reg_id"><div style="">1</div></td><td data-column="rds_account"><div style="float:right;">319</div></td><td data-column="rds_store"><div style="float:right;">373</div></td><td data-column="rds_intel"><div style="float:right;">0</div></td><td data-column="rds_mss"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="2" data-column="reg_id"><div style="">2</div></td><td data-column="rds_account"><div style="float:right;">378</div></td><td data-column="rds_store"><div style="float:right;">533</div></td><td data-column="rds_intel"><div style="float:right;">6,703</div></td><td data-column="rds_mss"><div style="float:right;color:red;">90.8 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="3" data-column="reg_id"><div style="">3</div></td><td data-column="rds_account"><div style="float:right;">158</div></td><td data-column="rds_store"><div style="float:right;">211</div></td><td data-column="rds_intel"><div style="float:right;">0</div></td><td data-column="rds_mss"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="4" data-column="reg_id"><div style="">4</div></td><td data-column="rds_account"><div style="float:right;">153</div></td><td data-column="rds_store"><div style="float:right;">205</div></td><td data-column="rds_intel"><div style="float:right;">5,559</div></td><td data-column="rds_mss"><div style="float:right;color:red;">66.4 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr>
					</tbody>			
				</table>
			</div>
		</div>	
	</div>
	
	<div class="col-md-8">                      
		<div class="block">
			<div class="block-content">
				<h2><strong>Visibility </strong></h2>
			</div>
			<div class="block-content np">
				<table class="table table-hover">		 			
					<thead><tr>
						<th>City</th><th>#VM</th><th>#Visit/VM QTD</th><th>#Display "OFF"</th><th>%Cling</th><th>#Display "ON"</th><th>%IPOS</th><th>%Store Merched</th>
					</tr></thead>
					<tbody><tr><td class="has-link" data-link="1" data-column="RegId"><div style="">1</div></td><td data-column="rdrVMQty"><div style="float:right;">8</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">168</div></td><td data-column="rdrDisplayOff"><div style="float:right;">2,261</div></td><td data-column="rdrPerCling"><div style="float:right;">67.7 % <i class="fa fa-plus-circle" style="color:green"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">378</div></td><td data-column="rdrPerIPos"><div style="float:right;">62.7 % <i class="fa fa-plus-circle" style="color:green"></i> </div></td><td data-column="rdrMerch"><div style="float:right;color:red;">27.9 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="2" data-column="RegId"><div style="">2</div></td><td data-column="rdrVMQty"><div style="float:right;">12</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">249</div></td><td data-column="rdrDisplayOff"><div style="float:right;">1,530</div></td><td data-column="rdrPerCling"><div style="float:right;">71.0 % <i class="fa fa-plus-circle" style="color:green"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">306</div></td><td data-column="rdrPerIPos"><div style="float:right;">88.9 % <i class="fa fa-plus-circle" style="color:green"></i> </div></td><td data-column="rdrMerch"><div style="float:right;color:red;">6.8 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="3" data-column="RegId"><div style="">3</div></td><td data-column="rdrVMQty"><div style="float:right;">4</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">236</div></td><td data-column="rdrDisplayOff"><div style="float:right;">591</div></td><td data-column="rdrPerCling"><div style="float:right;">99.8 % <i class="fa fa-plus-circle" style="color:green"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">9</div></td><td data-column="rdrPerIPos"><div style="float:right;color:red;">22.2 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td><td data-column="rdrMerch"><div style="float:right;color:red;">1.4 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="4" data-column="RegId"><div style="">4</div></td><td data-column="rdrVMQty"><div style="float:right;">4</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">190</div></td><td data-column="rdrDisplayOff"><div style="float:right;">436</div></td><td data-column="rdrPerCling"><div style="float:right;">97.5 % <i class="fa fa-plus-circle" style="color:green"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">11</div></td><td data-column="rdrPerIPos"><div style="float:right;">100.0 % <i class="fa fa-plus-circle" style="color:green"></i> </div></td><td data-column="rdrMerch"><div style="float:right;color:red;">3.9 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr>
					</tbody>
				</table>
			</div>
		</div>	
	</div>

	<div class="col-md-8">                      
		<div class="block">
			<div class="block-content">
				<h2><strong>Training </strong>- Product Update</h2>
			</div>
			<div class="block-content np">
				<table class="table table-bordered table-hover table-striped">
			<thead><tr><th rowspan="2" style="width:90px">City</th>
			<th rowspan="2" >#AM</th><th rowspan="2" >#Visit/AM QTD</th>
			<th rowspan="2" >#RSP</th><th rowspan="2" style="width:40px">#RSP Trained</th>
			<th colspan="6" style=" text-align:center">Topic</th><th rowspan="2" style="width:80px">%Store Trained</th>
			</tr><tr><th >#ITP</th><th >#Promo</th><th >#Tablet</th>
			<th >#PC</th><th >#DIY</th><th >#Total</th>
			</tr></thead>
			<tbody><tr><td class="has-link" data-link="1" data-column="RegId"><div style="">1</div></td><td data-column="rdt_am_qty"><div style="float:right;">8</div></td><td data-column="rdt_am_visit"><div style="float:right;">67</div></td><td data-column="rdt_total_rsp"><div style="float:right;">2,801</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">1,884</div></td><td data-column="rdt_total_itp"><div style="float:right;">40</div></td><td data-column="rdt_total_promo"><div style="float:right;">80</div></td><td data-column="rdt_total_tablet"><div style="float:right;">6</div></td><td data-column="rdt_total_pc"><div style="float:right;">487</div></td><td data-column="rdt_total_diy"><div style="float:right;">79</div></td><td data-column="rdt_total"><div style="float:right;">692</div></td><td data-column="rdt_per_store_trained"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="2" data-column="RegId"><div style="">2</div></td><td data-column="rdt_am_qty"><div style="float:right;">10</div></td><td data-column="rdt_am_visit"><div style="float:right;">29</div></td><td data-column="rdt_total_rsp"><div style="float:right;">1,087</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">831</div></td><td data-column="rdt_total_itp"><div style="float:right;">28</div></td><td data-column="rdt_total_promo"><div style="float:right;">48</div></td><td data-column="rdt_total_tablet"><div style="float:right;">26</div></td><td data-column="rdt_total_pc"><div style="float:right;">233</div></td><td data-column="rdt_total_diy"><div style="float:right;">75</div></td><td data-column="rdt_total"><div style="float:right;">410</div></td><td data-column="rdt_per_store_trained"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="3" data-column="RegId"><div style="">3</div></td><td data-column="rdt_am_qty"><div style="float:right;">4</div></td><td data-column="rdt_am_visit"><div style="float:right;">15</div></td><td data-column="rdt_total_rsp"><div style="float:right;">574</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">268</div></td><td data-column="rdt_total_itp"><div style="float:right;">0</div></td><td data-column="rdt_total_promo"><div style="float:right;">3</div></td><td data-column="rdt_total_tablet"><div style="float:right;">24</div></td><td data-column="rdt_total_pc"><div style="float:right;">19</div></td><td data-column="rdt_total_diy"><div style="float:right;">36</div></td><td data-column="rdt_total"><div style="float:right;">82</div></td><td data-column="rdt_per_store_trained"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="4" data-column="RegId"><div style="">4</div></td><td data-column="rdt_am_qty"><div style="float:right;">4</div></td><td data-column="rdt_am_visit"><div style="float:right;">7</div></td><td data-column="rdt_total_rsp"><div style="float:right;">133</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">68</div></td><td data-column="rdt_total_itp"><div style="float:right;">4</div></td><td data-column="rdt_total_promo"><div style="float:right;">0</div></td><td data-column="rdt_total_tablet"><div style="float:right;">12</div></td><td data-column="rdt_total_pc"><div style="float:right;">11</div></td><td data-column="rdt_total_diy"><div style="float:right;">15</div></td><td data-column="rdt_total"><div style="float:right;">42</div></td><td data-column="rdt_per_store_trained"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr>
			</tbody>
			</table>
		</div>
		</div>	
	</div>
	</div><!--row closed-->
	
</div>


<div class="row page-toolbar-tab" id="page-tab-2">
	<div class="row">
	<div class="col-md-8">
		<div class="block">
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a href="ui-tabs.html#tab8" data-toggle="tab">Sales Out</a></li>
				<li class=""><a href="ui-tabs.html#tab9" data-toggle="tab">Visibility</a></li>
				<li class=""><a href="ui-tabs.html#tab10" data-toggle="tab">Training</a></li>
			</ul>
			<div class="block-content tab-content">
			
				<div class="tab-pane active" id="tab8">
		                    
					<div class="block">
						<div class="block-content">
							<h2><strong>Sales </strong>Out</h2>
						</div>
						<div class="block-content np">
							<table class="table table-hover table-striped">
								<thead>				
								<tr>
									<th>City</th><th>#Acc</th><th>#Store</th><th>#Intel</th><th>%MSS</th><th>#QTD PQ</th><th>%QoQ</th><th>#QTD PY</th><th>%YoY</th>
								</tr>
								</thead>
								<tbody><tr><td class="has-link" data-link="7" data-column="CitName"><div style="text-align:left;">Bandung</div></td><td data-column="rds_account"><div style="float:right;">79</div></td><td data-column="rds_store"><div style="float:right;">102</div></td><td data-column="rds_intel"><div style="float:right;">0</div></td><td data-column="rds_mss"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="5" data-column="CitName"><div style="text-align:left;">Bekasi</div></td><td data-column="rds_account"><div style="float:right;">2</div></td><td data-column="rds_store"><div style="float:right;">4</div></td><td data-column="rds_intel"><div style="float:right;">0</div></td><td data-column="rds_mss"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="6" data-column="CitName"><div style="text-align:left;">Cilegon</div></td><td data-column="rds_account"><div style="float:right;">2</div></td><td data-column="rds_store"><div style="float:right;">0</div></td><td data-column="rds_intel"><div style="float:right;">0</div></td><td data-column="rds_mss"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="2" data-column="CitName"><div style="text-align:left;">Depok</div></td><td data-column="rds_account"><div style="float:right;">3</div></td><td data-column="rds_store"><div style="float:right;">0</div></td><td data-column="rds_intel"><div style="float:right;">0</div></td><td data-column="rds_mss"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="1" data-column="CitName"><div style="text-align:left;">Jakarta</div></td><td data-column="rds_account"><div style="float:right;">215</div></td><td data-column="rds_store"><div style="float:right;">229</div></td><td data-column="rds_intel"><div style="float:right;">0</div></td><td data-column="rds_mss"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="19" data-column="CitName"><div style="text-align:left;">Sukabumi</div></td><td data-column="rds_account"><div style="float:right;">1</div></td><td data-column="rds_store"><div style="float:right;">1</div></td><td data-column="rds_intel"><div style="float:right;">0</div></td><td data-column="rds_mss"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="4" data-column="CitName"><div style="text-align:left;">Tangerang</div></td><td data-column="rds_account"><div style="float:right;">17</div></td><td data-column="rds_store"><div style="float:right;">35</div></td><td data-column="rds_intel"><div style="float:right;">0</div></td><td data-column="rds_mss"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rds_qtd1"><div style="float:right;">0</div></td><td data-column="rds_qoq"><div style="float:right;">0.0 %</div></td><td data-column="rds_qtd_py"><div style="float:right;">0</div></td><td data-column="rds_yoy"><div style="float:right;">0.0 %</div></td></tr>
								</tbody>		
							</table>
						</div>
					</div>	
				</div>
				
				<div class="tab-pane" id="tab9">
					<div class="block">
					<div class="block-content">
						<h2><strong>Visibility </strong></h2>
					</div>
					<div class="block-content np">
						<table class="table table-hover">		 			
							<thead><tr>
								<th>City</th><th>#VM</th><th>#Visit/VM QTD</th><th>#Display "OFF"</th><th>%Cling</th><th>#Display "ON"</th><th>%IPOS</th><th>%Store Merched</th>
							</tr></thead>
							<tbody><tr><td class="has-link" data-link="7" data-column="CitName"><div style="text-align:left;">Bandung</div></td><td data-column="rdrVMQty"><div style="float:right;">1</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">393</div></td><td data-column="rdrDisplayOff"><div style="float:right;">552</div></td><td data-column="rdrPerCling"><div style="float:right;">81.5 % <i class="fa fa-plus-circle" style="color:green"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">20</div></td><td data-column="rdrPerIPos"><div style="float:right;">90.0 % <i class="fa fa-plus-circle" style="color:green"></i> </div></td><td data-column="rdrMerch"><div style="float:right;">50.0 % <i class="fa fa-plus-circle" style="color:green"></i> </div></td></tr><tr><td class="has-link" data-link="5" data-column="CitName"><div style="text-align:left;">Bekasi</div></td><td data-column="rdrVMQty"><div style="float:right;">1</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">22</div></td><td data-column="rdrDisplayOff"><div style="float:right;">14</div></td><td data-column="rdrPerCling"><div style="float:right;color:red;">35.7 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">20</div></td><td data-column="rdrPerIPos"><div style="float:right;color:red;">40.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td><td data-column="rdrMerch"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="6" data-column="CitName"><div style="text-align:left;">Cilegon</div></td><td data-column="rdrVMQty"><div style="float:right;">1</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">0</div></td><td data-column="rdrDisplayOff"><div style="float:right;">0</div></td><td data-column="rdrPerCling"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">0</div></td><td data-column="rdrPerIPos"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td><td data-column="rdrMerch"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="2" data-column="CitName"><div style="text-align:left;">Depok</div></td><td data-column="rdrVMQty"><div style="float:right;">1</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">0</div></td><td data-column="rdrDisplayOff"><div style="float:right;">0</div></td><td data-column="rdrPerCling"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">0</div></td><td data-column="rdrPerIPos"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td><td data-column="rdrMerch"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="1" data-column="CitName"><div style="text-align:left;">Jakarta</div></td><td data-column="rdrVMQty"><div style="float:right;">1</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">786</div></td><td data-column="rdrDisplayOff"><div style="float:right;">1,309</div></td><td data-column="rdrPerCling"><div style="float:right;">67.8 % <i class="fa fa-plus-circle" style="color:green"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">283</div></td><td data-column="rdrPerIPos"><div style="float:right;">64.3 % <i class="fa fa-plus-circle" style="color:green"></i> </div></td><td data-column="rdrMerch"><div style="float:right;color:red;">41.9 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="19" data-column="CitName"><div style="text-align:left;">Sukabumi</div></td><td data-column="rdrVMQty"><div style="float:right;">1</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">6</div></td><td data-column="rdrDisplayOff"><div style="float:right;">0</div></td><td data-column="rdrPerCling"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">0</div></td><td data-column="rdrPerIPos"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td><td data-column="rdrMerch"><div style="float:right;color:red;">0.0 % <i class="fa fa-minus-circle" style="color:red"></i> </div></td></tr><tr><td class="has-link" data-link="4" data-column="CitName"><div style="text-align:left;">Tangerang</div></td><td data-column="rdrVMQty"><div style="float:right;">1</div></td><td data-column="rdrVMPerVisit"><div style="float:right;">133</div></td><td data-column="rdrDisplayOff"><div style="float:right;">386</div></td><td data-column="rdrPerCling"><div style="float:right;color:red;">48.4 % <i class="fa fa-minus-circle" style="color:red"></i></div></td><td data-column="rdrDisplayOn"><div style="float:right;">31</div></td><td data-column="rdrPerIPos"><div style="float:right;">58.1 % <i class="fa fa-plus-circle" style="color:green"></i> </div></td><td data-column="rdrMerch"><div style="float:right;">51.4 % <i class="fa fa-plus-circle" style="color:green"></i> </div></td></tr>
							</tbody>
						</table>
					</div>
					</div>
				</div>
				<div class="tab-pane" id="tab10">
					<div class="block">
						<div class="block-content">
							<h2><strong>Training - </strong>Product Update</h2>
						</div>
						<div class="block-content np">
							<table class="table table-bordered table-hover table-striped">
						<thead><tr><th rowspan="2" style="width:90px">City</th>
						<th rowspan="2" >#AM</th>
						<th rowspan="2" >#Visit/AM QTD</th>
						<th rowspan="2" >#RSP</th><th rowspan="2" style="width:40px">#RSP Trained</th>
						<th colspan="6" style=" text-align:center">Topic</th><th rowspan="2" style="width:80px">%Store Trained</th>
						</tr><tr><th >#ITP</th><th >#Promo</th><th >#Tablet</th>
						<th >#PC</th><th >#DIY</th><th >#Total</th>
						</tr></thead>
						<tbody><tr><td class="has-link" data-link="7" data-column="CitName"><div style="text-align:left;">Bandung</div></td><td data-column="rdt_am_qty"><div style="float:right;">3</div></td><td data-column="rdt_am_visit"><div style="float:right;">28</div></td><td data-column="rdt_total_rsp"><div style="float:right;">415</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">260</div></td><td data-column="rdt_total_itp"><div style="float:right;">10</div></td><td data-column="rdt_total_promo"><div style="float:right;">21</div></td><td data-column="rdt_total_tablet"><div style="float:right;">0</div></td><td data-column="rdt_total_pc"><div style="float:right;">53</div></td><td data-column="rdt_total_diy"><div style="float:right;">26</div></td><td data-column="rdt_total"><div style="float:right;">0</div></td><td data-column="rdt_per_store_trained"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="5" data-column="CitName"><div style="text-align:left;">Bekasi</div></td><td data-column="rdt_am_qty"><div style="float:right;">2</div></td><td data-column="rdt_am_visit"><div style="float:right;">4</div></td><td data-column="rdt_total_rsp"><div style="float:right;">36</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">29</div></td><td data-column="rdt_total_itp"><div style="float:right;">0</div></td><td data-column="rdt_total_promo"><div style="float:right;">2</div></td><td data-column="rdt_total_tablet"><div style="float:right;">1</div></td><td data-column="rdt_total_pc"><div style="float:right;">12</div></td><td data-column="rdt_total_diy"><div style="float:right;">1</div></td><td data-column="rdt_total"><div style="float:right;">0</div></td><td data-column="rdt_per_store_trained"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="6" data-column="CitName"><div style="text-align:left;">Cilegon</div></td><td data-column="rdt_am_qty"><div style="float:right;">1</div></td><td data-column="rdt_am_visit"><div style="float:right;">9</div></td><td data-column="rdt_total_rsp"><div style="float:right;">12</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">7</div></td><td data-column="rdt_total_itp"><div style="float:right;">0</div></td><td data-column="rdt_total_promo"><div style="float:right;">2</div></td><td data-column="rdt_total_tablet"><div style="float:right;">1</div></td><td data-column="rdt_total_pc"><div style="float:right;">12</div></td><td data-column="rdt_total_diy"><div style="float:right;">1</div></td><td data-column="rdt_total"><div style="float:right;">0</div></td><td data-column="rdt_per_store_trained"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="2" data-column="CitName"><div style="text-align:left;">Depok</div></td><td data-column="rdt_am_qty"><div style="float:right;">2</div></td><td data-column="rdt_am_visit"><div style="float:right;">184</div></td><td data-column="rdt_total_rsp"><div style="float:right;">12</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">6</div></td><td data-column="rdt_total_itp"><div style="float:right;">30</div></td><td data-column="rdt_total_promo"><div style="float:right;">53</div></td><td data-column="rdt_total_tablet"><div style="float:right;">5</div></td><td data-column="rdt_total_pc"><div style="float:right;">341</div></td><td data-column="rdt_total_diy"><div style="float:right;">49</div></td><td data-column="rdt_total"><div style="float:right;">0</div></td><td data-column="rdt_per_store_trained"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="1" data-column="CitName"><div style="text-align:left;">Jakarta</div></td><td data-column="rdt_am_qty"><div style="float:right;">6</div></td><td data-column="rdt_am_visit"><div style="float:right;">61</div></td><td data-column="rdt_total_rsp"><div style="float:right;">1,985</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">1,325</div></td><td data-column="rdt_total_itp"><div style="float:right;">30</div></td><td data-column="rdt_total_promo"><div style="float:right;">53</div></td><td data-column="rdt_total_tablet"><div style="float:right;">5</div></td><td data-column="rdt_total_pc"><div style="float:right;">341</div></td><td data-column="rdt_total_diy"><div style="float:right;">49</div></td><td data-column="rdt_total"><div style="float:right;">0</div></td><td data-column="rdt_per_store_trained"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="19" data-column="CitName"><div style="text-align:left;">Sukabumi</div></td><td data-column="rdt_am_qty"><div style="float:right;">1</div></td><td data-column="rdt_am_visit"><div style="float:right;">12</div></td><td data-column="rdt_total_rsp"><div style="float:right;">6</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">5</div></td><td data-column="rdt_total_itp"><div style="float:right;">3</div></td><td data-column="rdt_total_promo"><div style="float:right;">1</div></td><td data-column="rdt_total_tablet"><div style="float:right;">1</div></td><td data-column="rdt_total_pc"><div style="float:right;">3</div></td><td data-column="rdt_total_diy"><div style="float:right;">8</div></td><td data-column="rdt_total"><div style="float:right;">0</div></td><td data-column="rdt_per_store_trained"><div style="float:right;">0.0 %</div></td></tr><tr><td class="has-link" data-link="4" data-column="CitName"><div style="text-align:left;">Tangerang</div></td><td data-column="rdt_am_qty"><div style="float:right;">3</div></td><td data-column="rdt_am_visit"><div style="float:right;">24</div></td><td data-column="rdt_total_rsp"><div style="float:right;">335</div></td><td data-column="rdt_total_rsp_trained"><div style="float:right;">252</div></td><td data-column="rdt_total_itp"><div style="float:right;">0</div></td><td data-column="rdt_total_promo"><div style="float:right;">4</div></td><td data-column="rdt_total_tablet"><div style="float:right;">0</div></td><td data-column="rdt_total_pc"><div style="float:right;">81</div></td><td data-column="rdt_total_diy"><div style="float:right;">3</div></td><td data-column="rdt_total"><div style="float:right;">0</div></td><td data-column="rdt_per_store_trained"><div style="float:right;">0.0 %</div></td></tr>
						</tbody>
						</table>
					</div>
					</div>	
				</div>                        
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="widget widget-success">
			<div class="widget-container">
				<div class="widget-text">Sales Out MSS %</div>
				<div class="widget-chart">
					<span class="sparkline" sparkType="pie"sparkHeight="80">100,0</span>
				</div>
			</div>
			<div class="widget-content">
				<div class="widget-text"><strong>Intel</strong> 100</div>
				<div class="widget-text"><strong>Amd</strong> 0</div>                                    
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="widget widget-warning">
			<div class="widget-container">
				<div class="widget-text">Visibility</div>
				<div class="widget-chart">
					<span class="sparkline" sparkType="pie"sparkHeight="80">72.1,27.9</span>
				</div>
			</div>
			<div class="widget-content">
				<div class="widget-text"><strong>Merched</strong> 72.1</div>
				<div class="widget-text"><strong>No Visibility</strong> 27.9</div>                                    
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="widget widget-info">
			<div class="widget-container">
				<div class="widget-text">Training Min 3 RSPs</div>
				<div class="widget-chart">
					<span class="sparkline" sparkType="pie"sparkHeight="80">0,100</span>
				</div>
			</div>
			<div class="widget-content">
				<div class="widget-text"><strong>Trained</strong> 0</div>
				<div class="widget-text"><strong>No Training</strong> 100</div>                                    
			</div>
		</div>
	</div>
	
	</div><!--row closed-->
</div>


@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/jvectormap/jquery-jvectormap-indonesia.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/knob/jquery.knob.js') }}"></script>
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>	
	<script type="text/javascript" src="{{ asset('themes/gemini/js/demo.js') }}"></script>	
@endsection

@section('scripts')

<script type="text/javascript">
    $(function(){
      $('#vector_map').vectorMap({
		map: 'indonesia_map',
		scaleColors: ['#C8EEFF', '#0071A4'],
		normalizeFunction: 'polynomial',
		hoverOpacity: 0.7,
		hoverColor: false,
		markerStyle: {
		  initial: {
			fill: '#F8E23B',
			stroke: '#383f47'
		  }
		},
		backgroundColor: '#383f47',
		markers: [
		  
		]
		
		});
    });
</script>

@endsection

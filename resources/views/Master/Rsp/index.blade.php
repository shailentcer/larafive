@extends('home')

@section('css')
<link href="{{ asset('/css/datatables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('page-toolbar')
	<div class="page-toolbar">
		<div class="page-toolbar-block">
			<div class="page-toolbar-title">RSP</div>
			<div class="page-toolbar-subtitle">Manage All RSP here</div>
		</div>
		<div class="page-toolbar-block pull-right">
			<div class="widget-info widget-from">
				<a href="{{URL::to('rsp/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Create</a>
				<!--a href="#" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a>
				<a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a-->
			</div>
		</div>
	</div>
@endsection

@section('page-content')
		
<div class="row">
<div class="col-md-12">
	<div class="block">
		<div class="block-content np">
		<table id="roles-table" class="table table-condensed">
			<thead>
			<tr>
				<th>Nama RSP</th>
				<th>Email</th>
				<th>Store</th>
				<th>Action</th>
			</tr>
			</thead>
		</table>
		</div>
	</div>
</div>
</div>
@endsection

@section('js')
	<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function() {
        $('#roles-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ url("rsp/data") }}',
			columns: [
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'store', name: 'store'},
            {data: 'action', name: 'action'}
			]
        });
    });
</script>
@endsection

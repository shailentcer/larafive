<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model {

	//
	protected $table = 'store';
	
	public function visibility()
	{
		/* 2nd argument is foreign key in parent table */
		return $this->hasMany('App\Models\Visibility');
	}
	
	public function training()
	{
		/* 2nd argument is foreign key in parent table */
		return $this->hasMany('App\Models\Training');
	}
	
	public function rsp()
	{
		/* 2nd argument is foreign key in parent table */
		return $this->hasMany('App\Models\Rsp');
	}
	
	public function kota()
	{
		/* 2nd argument is foreign key in child (this!) table */
		return $this->belongsTo('App\Models\Kota'); 
	}
	
	public function user()
	{
		return $this->belongsTo('App\User'); 
	}
	
	public function siUser(){
		return $this->belongsTo('App\User', 'si_id'); 
	}
}

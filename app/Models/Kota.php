<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model {

	//
	protected $table = 'kota';
	
	public function store()
	{
		/* 2nd argument is foreign key in parent table */
		return $this->hasMany('App\Models\Store');
	}
	
	public function regional()
	{
		/* 2nd argument is foreign key in child (this!) table */
		return $this->belongsTo('App\Models\Regional'); 
	}
	
	public function visibility()
    {
        return $this->hasManyThrough('App\Models\Visibility', 'App\Models\Store');
    }
	
}

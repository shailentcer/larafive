<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class zone extends Model {

	protected $table = 'zone';
	
	public function subActivityVisibility()
	{
		/* 2nd argument is foreign key in parent table */
		return $this->hasMany('App\Models\Sub_activity_visibility');
	}

}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Materi_training extends Model {
	protected $table = "materi_training";
	//
	
	public function kategoriTraining()
	  {
		/* 2nd argument is foreign key in parent table */
		return $this->belongsTo('App\Models\Kategori_training');
	  }
	  
	 public function training()
    {
        return $this->hasOne('App\Models\Training');
    }
	

}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Training extends Model {

	protected $table = 'training';
	
	public function store()
	{
		return $this->belongsTo('App\Models\store'); 
	}
	
	public function rsp()
    {
        return $this->belongsToMany('App\Models\Rsp');
    }
	
	public function materiTraining()
	  {
		/* 2nd argument is foreign key in parent table */
		return $this->belongsTo('App\Models\Materi_training');
	  }
	
}

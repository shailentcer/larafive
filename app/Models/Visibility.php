<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visibility extends Model {

	protected $table = 'visibility';
	// protected $primaryKey='id';
	
	public function user()
  {
    /* 2nd argument is foreign key in child (this!) table */
    return $this->belongsTo('App\User');
  }
	public function store()
  {
    /* 2nd argument is foreign key in child (this!) table */
    return $this->belongsTo('App\Models\Store');
  }
  public function subActivityVisibility()
  {
    /* 2nd argument is foreign key in child (this!) table */
    return $this->belongsTo('App\Models\Sub_activity_visibility');
  }
	
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regional extends Model {

	//
	protected $table = 'regional';
	
	public function kota()
	{
		/* 2nd argument is foreign key in parent table */
		return $this->hasMany('App\Models\Kota');
	}
	
	public function store()
    {
        return $this->hasManyThrough('App\Models\Store', 'App\Models\Kota');
    }
	
}

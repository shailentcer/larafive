<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori_training extends Model {
	protected $table = 'kategori_training';
	//
	public function materiTraining()
    {
        return $this->hasMany('App\Models\Materi_training');
    }
	
}

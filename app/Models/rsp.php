<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rsp extends Model {

	//
	protected $table = 'rsp';
	
	public function store()
    {
        return $this->belongsTo('App\Models\Store');
    }
	
	public function training()
    {
        return $this->belongsToMany('App\Models\Training');
    }
	
}

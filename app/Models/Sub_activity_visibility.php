<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sub_activity_visibility extends Model {

	protected $table = 'sub_activity_visibility';
	//
	public function visibility()
	{
		/* 2nd argument is foreign key in parent table */
		return $this->hasMany('App\Models\Visibility');
	}
	
	public function activityVisibility()
  {
    /* 2nd argument is foreign key in child (this!) table */
    return $this->belongsTo('App\Models\Activity_visibility');
  }
	public function zone()
  {
    /* 2nd argument is foreign key in child (this!) table */
    return $this->belongsTo('App\Models\Zone');
  }

}

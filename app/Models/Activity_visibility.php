<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity_visibility extends Model {
	
	protected $table = 'activity_visibility';
	//
	public function subActivityVisibility()
	{
		/* 2nd argument is foreign key in parent table */
		return $this->hasMany('App\Models\Sub_activity_visibility');
	}
	
	public function visibility()
    {
        return $this->hasManyThrough('App\Models\Visibility', 'App\Models\Sub_activity_visibility');
    }
	
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Permission;
use App\Role;
use Datatables;
use Validator, Input, Redirect; 

use Illuminate\Http\Request;

class UserController extends Controller {
	
	protected $user;
    protected $role;
    protected $permission;
	
	public function __construct(User $user, Role $role, Permission $permission)
    {
        $this->middleware('auth',['except' => ['getData']]);
		$this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return view('user/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{	$roles = $this->role->all();
		//foreach($roles as $role){echo $role['id'];}
		//exit;
		return view('user/create')->with('roles',$roles);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		$rules = [	'name' => 'required',
					'email' => 'required|email',
					'password' => 'required', 'role_id' => 'required', 'web_access' => 'required',
					'mobile_access' => 'required', 'activated' => 'required'];
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			 return Redirect::back()
                ->withErrors($validator) // send back all errors to previous form
                ->withInput();
		}
		else{
		$user = new User ();
        $user -> name = Input::get('name');
        $user -> email = Input::get('email');
        $user -> password = bcrypt(Input::get('password'));
		$user -> web_access = Input::get('web_access');
		$user -> mobile_access = Input::get('mobile_access');
		$user -> activated = Input::get('activated');
        $user -> save();
		$user->attachRole(Input::get('role_id'));
		return Redirect::to('/user');
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getData(){
		$users = User::leftjoin('role_user', 'role_user.user_id', '=', 'users.id')
		->leftjoin('roles', 'roles.id', '=', 'role_user.role_id')
		->select(['users.id', 'users.name','email', 'activated', 'roles.name as rolename', 'web_access', 'mobile_access']);
		//echo $users->get();
		//exit;
        return Datatables::of($users)
		->removeColumn('id')
		->editColumn('activated','@if($activated)
                            Yes
                        @else
                            No
                        @endif')
		->editColumn('web_access','@if($web_access)
                            Yes
                        @else
                            No
                        @endif')
		->editColumn('mobile_access','@if($mobile_access)
                            Yes
                        @else
                            No
                        @endif')
		->addColumn('action','<a href="{{ URL::to( \'user/edit/\'.$id) }}"class="btn btn-xs btn-primary">
							<i class="fa fa-pencil"></i> Edit</a>
							<a href="{{ URL::to( \'user/delete/\'.$id) }}"class="btn btn-xs btn-danger">
							<i class="fa fa-trash-o"></i> Delete</a>
							<a href="{{ URL::to( \'user/delete/\'.$id) }}"class="btn btn-xs btn-default">
							<i class="fa fa-key"></i> Reset Password</a>
                ')
		->make(true);
	}

}

<?php namespace App\Http\Controllers;

use App\Models\Visibility;
use App\Models\Training;
use App\Models\Regional;
use App\Models\Kota;
use App\User;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
	protected $visibility;
	protected $training;
	protected $regional;
	protected $kota;
	protected $user;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Visibility $visibility, Training $training, Regional $regional, Kota $kota, User $user)
	{
		$this->visibility = $visibility;
		$this->training = $training;
		$this->regional = $regional;
		$this->kota = $kota;
		$this->user = $user;
		$this->middleware('auth');
	}
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	 // N = jumlah/total
	 //diagram : N store/region, N channel/region(not confirmed)
	 //tabel visibility : N vm/region, N visit/region, N displayoff/region, %cling, N display On, %IPOS, %store merched(?????)
	//--------------------------------------SAHABAT INTEL---------------------------------------------------
	//tabel training : N SI, N Visit, N Rsp, N Rsp Trained, TOPIC : N Itp, N Promo, N Tablet, N PC, N DIY, N TOTAL, % Store Trained
	public function index()
	{	
		//$query = $this->visibility->all();
		$regions_array = array(); //parameter passing ke view
		$store_num = array(); //jumlah toko/region 
		$nama_region = array(); //nama region
		$vm_num = array(); //jumlah vm/region
		$visit_num = array();//jumlah visit/region
		$display_off = array(); //jumlah displayoff/region
		$moncling = array(); //persentase moncling       
		$display_on = array(); //jumlah displayon/region
		$ipos = array(); //persentase ipos 	
		
		//Var SI
		$si_num = array();
		$training_num = array();
		$rsp_trained = array();
		$itp_num = array();
		$promo_num = array();
		$tablet_num = array();
		$pc_num = array();
		$diy_num = array();
		$total_materi = array();
		
		//Store
		
		$regions = $this->regional->where('aktif','1')->get(); //all regions aktif
		$region_num = count($regions);
		$i=1;
		foreach($regions as $region){
			$visit_num[$i] = 0;
			$display_off[$i] = 0;
			$display_on[$i] = 0;
			$moncling[$i] = 0;
			$ipos[$i] = 0;
			
			$training_num[$i] = 0;
			$rsp_num[$i] = 0;
			$rsp_trained[$i] = 0;
			$itp_num[$i] = 0;
			$promo_num[$i] = 0;
			$tablet_num[$i] = 0;
			$pc_num[$i] = 0;
			$diy_num[$i] = 0;
			$total_materi[$i] = 0;
			
			$nama_region[$i] = $region->name;
			$store_num[$i] =  $region->store->where('aktif',1)->count();
			
			$stores = $region->store->where('aktif',1);
			$vm_num[$i] = count(array_unique($stores->lists('user_id')));
			$si_num[$i] = count(array_unique($stores->lists('si_id')));

			foreach($stores as $store ){
				$visit_num[$i] += $store->visibility->count();
				$training_num[$i] += $store->training->count();
				$rsp_num[$i] += $store->rsp->count();
				
				foreach($store->visibility->where('sub_activity_visibility_id',1) as $vb){
					$display_on[$i] += $vb->display_on;
					$ipos[$i] += $vb->jumlah_baru;
				}
				foreach($store->visibility->where('sub_activity_visibility_id',2) as $vb){
					$display_off[$i] += $vb->display_off;
					$moncling[$i] += $vb->jumlah_baru;
				}
				foreach($store->training as $training){
					$rsp_trained[$i] += $training->jumlah_peserta;
					switch($training->materiTraining->kategoriTraining->id) {
						case 1: $diy_num[$i]++; break; //DIY
						case 2: $itp_num[$i]++;  break; //ITP
						case 3: $pc_num[$i]++; break; //PC
						case 4: $promo_num[$i]++;  break;	//Promo
						case 5: $tablet_num[$i]++; break; //Tablet
					}
					
				}
				$total_materi[$i] = $itp_num[$i] + $promo_num[$i] + $pc_num[$i] + $tablet_num[$i] + $diy_num[$i];
		
			}
			//echo "|--------------------------------------";
			//echo $rsp_trained[$i];
			//echo $vm_num[$i]; 
			//echo $display_off[$i]."|";
			//foreach($datas->store as $data ){echo $data->visibility;}
			$i++;
		}
		$total_visit = array_sum($visit_num);
		$total_store = array_sum($store_num);
		$total_vm = array_sum($vm_num);
		
		$total_training = array_sum($training_num);
		$total_si = array_sum($si_num);
		//exit();
		//$moncling = round(($moncling/$display_off)*100,1);
		//$ipos =  round(($ipos/$display_)*100,1);
		

		
		return view('dashboard')->with(compact(['total_visit', 'total_vm', 'total_store','region_num','nama_region','store_num','vm_num',
												'visit_num', 'display_off', 'moncling', 'display_on', 'ipos']))
								->with(compact(['total_si','total_training','si_num', 'training_num', 'rsp_num', 'rsp_trained', 'itp_num', 'promo_num', 'pc_num',
												'tablet_num','diy_num', 'total_materi']));
	}
	
	public function index2()
	{
		return view('performance');
	}
	
	public function index3()
	{
		return view('profile');
	}

}

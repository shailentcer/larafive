<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Datatables;

class DemoController extends Controller
{

    public function getIndex()
    {
        return view('demo');
    }

    public function getData()
    {
        $users = User::select(['id', 'name', 'email', 'created_at', 'updated_at']);

        return Datatables::of($users)->make(true);
    }

}
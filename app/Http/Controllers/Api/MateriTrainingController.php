<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Materi_training;
use Response;

class MateriTrainingController extends Controller {

	public function getListMateri($kategori_id){
		$materials = Materi_training::where('kategori_training_id',$kategori_id)->where('aktif',1)->get();
		if(count($materials) > 0){
			$result = array();
			$data = array();
			$data = $materials->toArray();
			$result['status'] = 200;
			$result['message'] = "OK";
			$result['data'] = $data;
			return Response::json($result);
		}
		else{
			$result = array();
			$data = array();
			$result['status'] = 204;
			$result['message'] = "Failed";
			$result['data'] = $data;
			return Response::json($result);
		}
	}
	
}

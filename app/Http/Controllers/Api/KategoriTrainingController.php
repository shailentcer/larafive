<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Kategori_training;
use Response;

class KategoriTrainingController extends Controller {

	public function getListKategori(){
		$categories = Kategori_training::where('aktif',1)->get();
		if(count($categories) > 0){
			$result = array();
			$data = array();
			$data = $categories->toArray();
			$result['status'] = 200;
			$result['message'] = "OK";
			$result['data'] = $data;
			return Response::json($result);
		}
		else{
			$result = array();
			$data = array();
			$result['status'] = 204;
			$result['message'] = "Failed";
			$result['data'] = $data;
			return Response::json($result);
		}
	}
	
}

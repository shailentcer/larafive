<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Sub_activity_visibility;
use Response;

class SubActivityVisibilityController extends Controller {
	
	public function getListSubActivity($id){
		$sub_activities = Sub_activity_visibility::where('activity_visibility_id',$id)->where('aktif',1)->get();
		if(count($sub_activities) > 0){
			$result = array();
			$data = array();
			$data = $sub_activities->toArray();
			$result['status'] = 200;
			$result['message'] = "OK";
			$result['data'] = $data;
			return Response::json($result);
		}
		else{
			$result = array();
			$data = array();
			$result['status'] = 204;
			$result['message'] = "Failed";
			$result['data'] = $data;
			return Response::json($result);
		}
	}
	
}

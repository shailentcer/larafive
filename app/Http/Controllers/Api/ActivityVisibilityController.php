<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Activity_visibility;
use Response;

class ActivityVisibilityController extends Controller {

	public function getListActivity(){
		$activities = Activity_visibility::where('aktif',1)->get();
		if(count($activities) > 0){
			$result = array();
			$data = array();
			$data = $activities->toArray();
			$result['status'] = 200;
			$result['message'] = "OK";
			$result['data'] = $data;
			return Response::json($result);
		}
		else{
			$result = array();
			$data = array();
			$result['status'] = 204;
			$result['message'] = "Failed";
			$result['data'] = $data;
			return Response::json($result);
		}
	}
}

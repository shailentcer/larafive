<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Response;
use Input;
use  Validator;


use Illuminate\Http\Request;

class AuthController extends Controller {

	public function postLogin(){
		$email = Input::get('email');
		$password = Input::get('password');
		 if (Auth::attempt(['email' => $email, 'password' => $password]))
        {	
			$user = User::where('email', $email)->get();
			//echo var_dump($user);
			$result = array();
			$userarray = array();
			$userarray = $user->toArray();
			$result['status'] = 200;
			$result['message'] = "OK";
			$result['data'] = $userarray;
			return Response::json($result);
        }
		else{
			$result = array();
			$userarray = array();
			$result['status'] = 204;
			$result['message'] = "Failed";
			$result['data'] = $userarray;
			return Response::json($result);
		}
	}
	public function postRegister(){
		$rules = [	'name' => 'required',
					'email' => 'required|email',
					'password' => 'required'];
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			$result = array();
			$userarray = array();
			$result['status'] = 204;
			$result['message'] = "Input Kurang atau Salah";
			$result['data'] = $userarray;
			return Response::json($result);
		}
		else{
		$user = new User ();
        $user -> name = Input::get('name');
        $user -> email = Input::get('email');
        $user -> password = bcrypt(Input::get('password'));
		// tambahin field attach role dan konfirmasi disini
        //$user -> confirmation_code = str_random(32);
        //$user -> confirmed = $request->confirmed;
        $user -> save();
		
		$result = array();
		$userarray = array();
		$userarray = $user->toArray();
		$result['status'] = 200;
		$result['message'] = "OK";
		$result['data'] = $userarray;
		return Response::json($result);
		}
	}

}

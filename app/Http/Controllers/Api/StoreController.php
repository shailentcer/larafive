<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Store;
use Response;

class StoreController extends Controller {

	public function getListStore($user_id){
		$stores = Store::where('user_id',$user_id)->get();
		if(count($stores) > 0){
			$result = array();
			$data = array();
			$data = $stores->toArray();
			$result['status'] = 200;
			$result['message'] = "OK";
			$data2 = array();
			$i=0;
			foreach($stores as $store){
				$data2[$i] = $store->kota->name;
				$i++;
			}
			for($i=0; $i<=1; $i++){
				$data[$i] = array_merge($data[$i],[ 'kota' => $data2[$i]]);
			}
			$result['data'] = $data;
			//var_dump($data);
			return Response::json($result);
			}
		else{
			$result = array();
			$data = array();
			$result['status'] = 204;
			$result['message'] = "Failed";
			$result['data'] = $data;
			return Response::json($result);
		}
	}
	
	public function getAllStore(){
		$stores = Store::all();
		if(count($stores) > 0){
			$result = array();
			$data = array();
			$data = $stores->toArray();
			$result['status'] = 200;
			$result['message'] = "OK";
			$data2 = array();
			$i=0;
			foreach($stores as $store){
				$data2[$i] = $store->kota->name;
				$i++;
			}
			for($i=0; $i<=1; $i++){
				$data[$i] = array_merge($data[$i],[ 'kota' => $data2[$i]]);
			}
			$result['data'] = $data;
			//var_dump($data);
			return Response::json($result);
			}
		else{
			$result = array();
			$data = array();
			$result['status'] = 204;
			$result['message'] = "Failed";
			$result['data'] = $data;
			return Response::json($result);
		}
	}

}

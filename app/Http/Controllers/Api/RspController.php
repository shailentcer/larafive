<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Rsp;
use Response;

class RspController extends Controller {

	public function getListRsp($store_id){
		$rsps = Rsp::where('store_id',$store_id)->get();
		if(count($rsps) > 0){
			$result = array();
			$data = array();
			$data = $rsps->toArray();
			$result['status'] = 200;
			$result['message'] = "OK";
			$result['data'] = $data;
			return Response::json($result);
		}
		else{
			$result = array();
			$data = array();
			$result['status'] = 204;
			$result['message'] = "Failed";
			$result['data'] = $data;
			return Response::json($result);
		}
	}
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Permission;

class fooController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		/*
		$owner = new Role();
		$owner->name         = 'owner';
		$owner->display_name = 'Project Owner'; // optional
		$owner->description  = 'User is the owner of a given project'; // optional
		$owner->save();

		$admin = new Role();
		$admin->name         = 'admin';
		$admin->display_name = 'User Administrator'; // optional
		$admin->description  = 'User is allowed to manage and edit other users'; // optional
		$admin->save();
		//-----------------------
		*/

		/*
		$user = User::where('name', '=', 'admin')->first();
		$admin = Role::where('name','=','admin')->first();
		// role attach alias
		$user->attachRole($admin); // parameter can be an Role object, array, or id
		//-----------------------------------------------
		*/
		/*
		$createUser = new Permission();
		$createUser->name         = 'create-user';
		$createUser->display_name = 'Create Users'; // optional
		// Allow a user to...
		$createUser->description  = 'create new users'; // optional
		$createUser->save();

		$editUser = new Permission();
		$editUser->name         = 'edit-user';
		$editUser->display_name = 'Edit Users'; // optional
		// Allow a user to...
		$editUser->description  = 'edit existing users'; // optional
		$editUser->save();
		
		$admin = Role::where('name','=','admin')->first();
		$admin->attachPermission(array($createUser, $editUser));
		// equivalent to $admin->perms()->sync(array($createPost->id));
		$owner = Role::where('name','=','owner')->first();
		$owner->attachPermissions(array($createUser, $editUser));
		// equivalent to $owner->perms()->sync(array($createPost->id, $editUser->id));
		*/
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use App\User;
use Datatables, Input, Validator, Redirect;

use Illuminate\Http\Request;

class RoleController extends Controller {

	protected $user;
    protected $role;
    protected $permission;
	
	public function __construct(User $user, Role $role, Permission $permission)
    {
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
    } 
	 
	public function getIndex()
	{
		return view('role/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$permissions = $this->permission->all();
		return view('role/create')->with('permissions',$permissions);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		//echo var_dump($this->permission->preparePermissionsForSave(Input::get('permissions')));
		//$role = $this->role->where('id',9)->first();
		//$role->perms()->sync($this->permission->preparePermissionsForSave(Input::get('permissions')));
		
		/*$createPost = new Permission();
		$createPost->name         = 'create-post';
		$createPost->display_name = 'Create Posts'; // optional
		$createPost->description  = 'create new blog posts'; // optional
		$createPost->save();

		$this->role->attachPermission($createPost);
		exit;*/
		$rules = [	'name' => 'required'];
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			 return Redirect::back()
                ->withErrors($validator) // send back all errors to previous form
                ->withInput();
		}
		else{
			//$inputs = Input::except('csrf_token');
            //$this->role->name = $inputs['name'];
            //$this->role->save();
			$this->role -> name = Input::get('name');
			$this->role -> display_name = Input::get('display_name');
			$this->role -> description = Input::get('description');
			$this->role -> save();
			
			 // Save permissions
			$this->role->perms()->sync($this->permission->preparePermissionsForSave(Input::get('permissions')));

            // Was the role created?
            if ($this->role->id)
            {
                return Redirect::to('/role')->with('success', 'Role Created Successfull');
            }
			return Redirect::to('role/create')->with('error','Failed');
			
		}

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getData(){
		$roles = Role::select(['id', 'name', 'display_name', 'description']);

        return Datatables::of($roles)
		->removeColumn('id')
		->addColumn('action','<a href="{{ URL::to( \'role/edit/\'.$id) }}"class="btn btn-xs btn-primary">
							<i class="fa fa-pencil"></i> Edit</a>
							<a href="{{ URL::to( \'role/delete/\'.$id) }}"class="btn btn-xs btn-danger">
							<i class="fa fa-trash-o"></i> Delete</a>
                ')
		->make(true);
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission;
use Datatables, Input, Redirect, Validator;

use Illuminate\Http\Request;

class PermissionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return view('Permission/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return view('permission/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
	
		$rules = [	'name' => 'required'];
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			 return Redirect::back()
                ->withErrors($validator) // send back all errors to previous form
                ->withInput();
		}
		else{
		$permission = new Permission ();
        $permission -> name = Input::get('name');
        $permission -> display_name = Input::get('display_name');
        $permission -> description = Input::get('description');
        $permission -> save();
		return Redirect::to('/permission');
		}

	}

	public function getEdit($id)
	{
		return view('permission/edit');
	}
	
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getData(){
		$permissions = Permission::select(['id','name', 'display_name', 'description']);

        return Datatables::of($permissions)
		->removeColumn('id')
		->addColumn('action','<a href="{{ URL::to( \'permission/edit/\'.$id) }}"class="btn btn-xs btn-primary">
							<i class="fa fa-pencil"></i> Edit</a>
							<a href="{{ URL::to( \'permission/delete/\'.$id) }}"class="btn btn-xs btn-danger">
							<i class="fa fa-trash-o"></i> Delete</a>
                ')
		->make(true);
	}

}

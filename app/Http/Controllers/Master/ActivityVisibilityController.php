<?php namespace App\Http\Controllers\Master;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Activity_visibility;
use Datatables, Input, Validator, Redirect;


class ActivityVisibilityController extends Controller {


	public function getIndex()
	{
		return view('master/brand/index');
	}
	
	public function getData(){
		$kota = Brand::select(['id', 'name']);
        return Datatables::of($kota)
		->removeColumn('id')
		->addColumn('action','<a href="{{ URL::to( \'brand/edit/\'.$id) }}"class="btn btn-xs btn-primary">
							<i class="fa fa-pencil"></i> Edit</a>
							<a href="{{ URL::to( \'brand/delete/\'.$id) }}"class="btn btn-xs btn-danger">
							<i class="fa fa-trash-o"></i> Delete</a>
                ')
		->make(true);
	}

}

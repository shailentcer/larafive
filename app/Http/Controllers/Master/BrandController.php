<?php namespace App\Http\Controllers\Master;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Brand;
use Datatables, Input, Validator, Redirect;

class BrandController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return view('master/brand/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function getData(){
		$kota = Brand::select(['id', 'name']);
        return Datatables::of($kota)
		->removeColumn('id')
		->addColumn('action','<a href="{{ URL::to( \'brand/edit/\'.$id) }}"class="btn btn-xs btn-primary">
							<i class="fa fa-pencil"></i> Edit</a>
							<a href="{{ URL::to( \'brand/delete/\'.$id) }}"class="btn btn-xs btn-danger">
							<i class="fa fa-trash-o"></i> Delete</a>
                ')
		->make(true);
	}

}

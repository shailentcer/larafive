<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/performance', 'HomeController@index2');
Route::get('/performance/{id}', 'HomeController@index3');
//Route::get('/vm', 'HomeController@index');

Route::controllers([
	'auth' 		=> 'Auth\AuthController',
	'password' 	=> 'Auth\PasswordController',
	'demo'      => 'DemoController',
	'user'		=> 'UserController',
	'role'		=> 'RoleController',
	'permission'=> 'PermissionController',
	
	'regional'=> 'Master\RegionalController',
	'kota'=> 'Master\KotaController',
	'brand'=> 'Master\BrandController',
	'rsp'=> 'Master\RspController',
	'subactivityvisibility' => 'Master\SubActivityVisibilityController',
	'activityvisibility' => 'Master\ActivityVisibilityController',
	'store' => 'Master\StoreController',
	'kategoritraining' => 'Master\KategoriTrainingController',
	'materitraining' => 'Master\MateriTrainingController',
	'kategorichannel' => 'Master\KategoriChannelController',
	'channel' => 'Master\ChannelController',
]);

Route::group(['prefix' => 'api/v1'], function() //, 'middleware' => ['auth']
{
    Route::get('/debug', function()
	{
		echo "HELLO API";
	});
	Route::post('auth/login', 'Api\AuthController@postLogin');
	Route::post('auth/register', 'Api\AuthController@postRegister');
	Route::get('store/list/{store_id}', 'Api\StoreController@getListStore');
	Route::get('store/all', 'Api\StoreController@getAllStore');
	Route::get('activity/list', 'Api\ActivityVisibilityController@getListActivity');
	Route::get('activity/batch', 'Api\ActivityVisibilityController@getBatchActivity');
	Route::get('subactivity/list/{act_id}', 'Api\SubActivityVisibilityController@getListSubActivity');
	Route::get('kategoritraining/list/', 'Api\KategoriTrainingController@getListKategori');
	Route::get('materitraining/list/{kategori_id}', 'Api\MateriTrainingController@getListMateri');
	Route::get('rsp/list/{store_id}', 'Api\RspController@getListRsp');
	
});

Route::get('/foo', 'fooController@index');

/*
Route::get('/debug', function()
{
	$users = DB::table('users')->select('name', 'email');
	dd($users);
    echo "executed";
});
*/
